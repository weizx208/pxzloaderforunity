using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Pixyz.Commons.Extensions;


public class GenericImporter
{
    private static string prefabPath;

    public static void ImportFile(string filePath, string prefabFilePath)
    {

        Debug.Log("Importing: " + filePath);
        prefabPath = prefabFilePath;

        try
        {
            var importer = new Pixyz.Loader.Runtime.Importer(filePath);
            importer.isAsynchronous = false;
            importer.completed += onImportEnded;
            importer.run();

        }
        catch (Pixyz.NoValidLicenseException)
        {
        }
    }

    static void onImportEnded(GameObject gameObject)
    {

        Debug.Log("Model Imported");

        //create prefab
        gameObject.CreatePrefab(prefabPath);

    }

    static void onProgressChanged(float progress, string message)
    {
        Debug.Log("Progress : " + 100f * progress + "%");
    }

}